# Config
_Config repo for fresh server install_

## Usage

For a standard install do 

    git clone https://gitlab.com/b238/Config
    cd Config
    ./run

For partial installs view the component install files in the `components` directory.

## Directories
* `bin` contains useful executable code for command line operations (it is on `PATH`)
* `components` contains the component installs for fresh install
* `etc` contains setup files such as bash_aliases, config files, and pubkeys



